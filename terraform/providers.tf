terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 5.0"
    }
  }
  backend "http" {
    address        = "https://gitlab.com/api/v4/projects/53500197/terraform/state/infra-tf-st"
    lock_address   = "https://gitlab.com/api/v4/projects/53500197/terraform/state/infra-tf-st/lock"
    unlock_address = "https://gitlab.com/api/v4/projects/53500197/terraform/state/infra-tf-st/lock"
  }
}

provider "aws" {
  region = "us-east-1"
}