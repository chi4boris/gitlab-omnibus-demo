variable "ami" {
  description = "ubuntu AMI to be used by EC2 instance"
  type        = string
  default     = "ami-0c7217cdde317cfec"
}

variable "instance_type" {
  description = "Define instance type"
  type        = string
  default     = "t2.xlarge"
}

variable "vpc_cidr_block" {
  description = "CIDR block for VPC"
  type        = string
  default     = "10.0.0.0/16"
}

variable "subnet_cidr" {
  description = "CIDR block for subnet"
  type        = string
  default     = "10.0.2.0/24"
}

variable "gitlab_cidr" {
  description = "The CIDR block of gitlab API/Web"
  type        = list(string)
  default     = ["0.0.0.0/0"] # "34.74.90.64/28", "34.74.226.0/24"
}

variable "tags" {
  description = "Infra identifier"
  type        = string
  default     = "gl-omnibus"
}